import "bootstrap"
import "./styles/index.scss"

import Vue from "vue"

import "~/plugins/bootstrap-vue"
import "~/plugins/vee-validate"

import Editor from "~/plugins/vue-editor-js/src"
Vue.use(Editor)

import App from "./app/editor.vue"

if (document.getElementById("app-editor")) {
  console.log("hello, editor")
  const editor = new Vue({
    el: "#app-editor",
    data() {
      return {
        version: 0.1,
        processSave: false,
        process: {},
      }
    },
    methods: {
      getEditor() {
        return this.$refs?.app?.$refs?.editor?._data?.state?.editor
      },
      async showPublichModal() {
        await this.$refs["app-editor"].showModal()
      },
      async save({ cover_id, published_at }) {
        if (this.processSave) return
        this.processSave = true

        try {
          const json = await this.$refs["app-editor"].getJson()
          const title = await this.$refs["app-editor"].getTitle()
          const postId = window?.POST?.id

          const form = { cover_id, published_at, title, json }

          if (postId) {
            await this.$axios.$post("/api/post/update", { id: postId, ...form })
            window.App.notify({
              type: "info",
              text: "Сохранено",
            })
          } else {
            const { id } = await this.$axios.$post("/api/post/create", {
              ...form,
            })
            window.location.href = `/posts/${id}`
          }
        } catch (e) {
          console.error(e)
        }
        this.processSave = false
      },
    },
    render: (h) => h(App, { ref: "app" }),
  })

  window.AppEditor = editor
}
