module.exports = {
  AUTH_URL: '/api/login',
  REGISTER_URL: '/api/register',
  RESTORE_URL: '/api/reset_mail',

  PROXY: 'http://slabber.local/'
};
