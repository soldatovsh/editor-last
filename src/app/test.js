export default {
  time: 1642317747553,
  blocks: [
    {
      id: 'PdehscATxR',
      type: 'sh-image-gallery',
      data: {
        urls: [
          {
            id: '332',
            url: 'http://slabber.local/static/upload/post/2022-01-14/a93c859c6d5a1840e9bc6ba1cb6ff953.jpg',
            width: 1200,
            height: 800,
            flex: 'flex: 1.5 1 0%'
          },
          {
            id: '333',
            url: 'http://slabber.local/static/upload/post/2022-01-14/643c214956d2363c0a6d7b952e906321.jpg',
            width: 1000,
            height: 666,
            flex: 'flex: 1.5015015015015014 1 0%'
          },
          {
            id: '334',
            url: 'http://slabber.local/static/upload/post/2022-01-14/36f718781fd848f1a9cb676d65b50e05.jpg',
            width: 599,
            height: 640,
            flex: 'flex: 0.9359375 1 0%'
          },
          {
            id: '335',
            url: 'http://slabber.local/static/upload/post/2022-01-14/abf14db6370f83e8886ae242d78fd2d2.jpg',
            width: 1600,
            height: 1280,
            flex: 'flex: 1.25 1 0%'
          }
        ],
        rows: [
          [
            {
              id: '332',
              url: 'http://slabber.local/static/upload/post/2022-01-14/a93c859c6d5a1840e9bc6ba1cb6ff953.jpg',
              width: 1200,
              height: 800,
              flex: 'flex: 1.5 1 0%'
            },
            {
              id: '333',
              url: 'http://slabber.local/static/upload/post/2022-01-14/643c214956d2363c0a6d7b952e906321.jpg',
              width: 1000,
              height: 666,
              flex: 'flex: 1.5015015015015014 1 0%'
            }
          ],
          [
            {
              id: '334',
              url: 'http://slabber.local/static/upload/post/2022-01-14/36f718781fd848f1a9cb676d65b50e05.jpg',
              width: 599,
              height: 640,
              flex: 'flex: 0.9359375 1 0%'
            },
            {
              id: '335',
              url: 'http://slabber.local/static/upload/post/2022-01-14/abf14db6370f83e8886ae242d78fd2d2.jpg',
              width: 1600,
              height: 1280,
              flex: 'flex: 1.25 1 0%'
            }
          ]
        ],
        stretched: true,
        images: [
          {
            id: '332',
            url: 'http://slabber.local/static/upload/post/2022-01-14/a93c859c6d5a1840e9bc6ba1cb6ff953.jpg',
            width: 1200,
            height: 800,
            flex: 'flex: 1.5 1 0%'
          },
          {
            id: '333',
            url: 'http://slabber.local/static/upload/post/2022-01-14/643c214956d2363c0a6d7b952e906321.jpg',
            width: 1000,
            height: 666,
            flex: 'flex: 1.5015015015015014 1 0%'
          },
          {
            id: '334',
            url: 'http://slabber.local/static/upload/post/2022-01-14/36f718781fd848f1a9cb676d65b50e05.jpg',
            width: 599,
            height: 640,
            flex: 'flex: 0.9359375 1 0%'
          },
          {
            id: '335',
            url: 'http://slabber.local/static/upload/post/2022-01-14/abf14db6370f83e8886ae242d78fd2d2.jpg',
            width: 1600,
            height: 1280,
            flex: 'flex: 1.25 1 0%'
          }
        ]
      }
    },
    {
      id: 'ixmMGurLub',
      type: 'paragraph',
      data: {
        text: 'парывпр ырпоырыорп оыврпыо роырп оырпыпыпп'
      }
    }
  ],
  version: '2.22.2'
};
