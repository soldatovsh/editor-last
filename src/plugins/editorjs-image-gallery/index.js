import Uploader from './uploader';
import Sortable, { Swap } from 'sortablejs';
Sortable.mount(new Swap());
// https://packery.metafizzy.co/
/**
 * Build styles
 */
// require('./index.css').toString();

const buttonIcon = `<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
    <path d="M3.15 13.628A7.749 7.749 0 0 0 10 17.75a7.74 7.74 0 0 0 6.305-3.242l-2.387-2.127-2.765 2.244-4.389-4.496-3.614 3.5zm-.787-2.303l4.446-4.371 4.52 4.63 2.534-2.057 3.533 2.797c.23-.734.354-1.514.354-2.324a7.75 7.75 0 1 0-15.387 1.325zM10 20C4.477 20 0 15.523 0 10S4.477 0 10 0s10 4.477 10 10-4.477 10-10 10z"/>
</svg>`;

const buttonTrashIcon = `<svg viewBox="0 0 16 16" class="fill-darkgrey w4 h4"><g fill="#FFF" fill-rule="nonzero"><path d="M1.3 4.6a.6.6 0 010-1.2h13.5a.6.6 0 010 1.2H1.3z"></path><path d="M13.9 4v9.5c0 1.331-.769 2.1-2.1 2.1H4.3c-1.331 0-2.1-.769-2.1-2.1V4a.6.6 0 01.6-.6h10.5a.6.6 0 01.6.6zM5.95 6.75v5.5a.6.6 0 001.2 0v-5.5a.6.6 0 00-1.2 0zm3 0v5.5a.6.6 0 001.2 0v-5.5a.6.6 0 00-1.2 0z"></path><path d="M5.65 4a.6.6 0 01-1.2 0V2.5c0-.573.21-1.08.615-1.485S5.977.4 6.55.4h3c1.331 0 2.1.769 2.1 2.1V4a.6.6 0 01-1.2 0V2.5c0-.669-.231-.9-.9-.9h-3a.843.843 0 00-.636.264.846.846 0 00-.264.636V4z"></path></g></svg>`;
/**
 * Build gallery grid
 */
// const gg = require('./gallery-grid/gallery-grid');

/**
 * ImageGallery Tool for the Editor.js
 * Works with image URLs and requires no server-side uploader.
 *
 * @typedef {object} ImageGalleryData
 * @property {string[]} urls — images URL
 * @property {boolean} editImages - activate or deactivate for show textarea with images urls
 * @property {boolean} bkgMode - activate or deactivate dark mode
 * @property {boolean} layoutDefault - should layout be the default
 * @property {boolean} layoutHorizontal - should layout be the horizontal
 * @property {boolean} layoutSquare - should layout be the square
 * @property {boolean} layoutWithGap - should layout be with gap
 * @property {boolean} layoutWithFixedSize - should layout be with fixed size
 */
class ImageGallery {
  static get toolbox() {
    return {
      title: 'Image Gallery',
      icon:
        '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-images" viewBox="0 0 16 16"><path d="M4.502 9a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/> <path d="M14.002 13a2 2 0 0 1-2 2h-10a2 2 0 0 1-2-2V5A2 2 0 0 1 2 3a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2v8a2 2 0 0 1-1.998 2zM14 2H4a1 1 0 0 0-1 1h9.002a2 2 0 0 1 2 2v7A1 1 0 0 0 15 11V3a1 1 0 0 0-1-1zM2.002 4a1 1 0 0 0-1 1v8l2.646-2.354a.5.5 0 0 1 .63-.062l2.66 1.773 3.71-3.71a.5.5 0 0 1 .577-.094l1.777 1.947V5a1 1 0 0 0-1-1h-10z"/></svg>'
    };
  }

  /**
   * Render plugin`s main Element and fill it with saved data
   *
   * @param {{data: ImageGalleryData, api: object}}
   *   data — previously saved data
   *   api - Editor.js API
   */
  constructor({ data, api, config }) {
    this.urls = data.images || [];
    this.rows = data.rows || [];
    /**
     * Tool's initial config
     */
    this.config = {
      endpoints: config.endpoints || '',
      additionalRequestData: config.additionalRequestData || {},
      additionalRequestHeaders: config.additionalRequestHeaders || {},
      field: config.field || 'image',
      types: config.types || 'image/*',
      captionPlaceholder: config.captionPlaceholder || 'Caption',
      buttonContent: config.buttonContent || '',
      uploader: config.uploader || undefined,
      actions: config.actions || []
    };

    /**
     * Nodes cache
     */
    this.nodes = {
      wrapper: null,
      button: null,
      urls: [],
      editImages: false,
      bkgMode: false,
      layoutDefault: false,
      layoutHorizontal: false,
      layoutSquare: false,
      layoutWithGap: true,
      layoutWithFixedSize: false
    };

    /**
     * Tool's initial data
     */
    this.data = {
      urls: data.images || [],
      rows: data.rows || [],
      stretched: true
      // editImages: data.editImages !== undefined ? data.editImages : true,
      // bkgMode: data.bkgMode !== undefined ? data.bkgMode : false,
      // layoutDefault: data.layoutDefault !== undefined ? data.layoutDefault : true,
      // layoutHorizontal: data.layoutHorizontal !== undefined ? data.layoutHorizontal : false,
      // layoutSquare: data.layoutSquare !== undefined ? data.layoutSquare : false,
      // layoutWithGap: true || data.layoutWithGap !== undefined ? data.layoutWithGap : false
      // layoutWithFixedSize: data.layoutWithFixedSize !== undefined ? data.layoutWithFixedSize : false,
    };

    /**
     * Editor.js API
     */
    this.api = api;
    this.wrapper = undefined;

    /**
     * Available Gallery settings
     */
    this.settings = [
      // {
      //     name: 'editImages',
      //     icon: `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16"><path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/><path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/></svg>`,
      //     title: 'Edit Images',
      // },
      // {
      //     name: 'bkgMode',
      //     icon: `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-layers-fill" viewBox="0 0 16 16"><path d="M7.765 1.559a.5.5 0 0 1 .47 0l7.5 4a.5.5 0 0 1 0 .882l-7.5 4a.5.5 0 0 1-.47 0l-7.5-4a.5.5 0 0 1 0-.882l7.5-4z"/><path d="m2.125 8.567-1.86.992a.5.5 0 0 0 0 .882l7.5 4a.5.5 0 0 0 .47 0l7.5-4a.5.5 0 0 0 0-.882l-1.86-.992-5.17 2.756a1.5 1.5 0 0 1-1.41 0l-5.17-2.756z"/></svg>`,
      //     title: 'Background Mode',
      // },
      // {
      //     name: 'layoutDefault',
      //     icon: `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-columns" viewBox="0 0 16 16"><path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V2zm8.5 0v8H15V2H8.5zm0 9v3H15v-3H8.5zm-1-9H1v3h6.5V2zM1 14h6.5V6H1v8z"/></svg>`,
      //     title: 'Default Layout',
      // },
      // {
      //     name: 'layoutHorizontal',
      //     icon: `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-grid-3x2-gap" viewBox="0 0 16 16"><path d="M4 4v2H2V4h2zm1 7V9a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zm0-5V4a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zm5 5V9a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zm0-5V4a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zM9 4v2H7V4h2zm5 0h-2v2h2V4zM4 9v2H2V9h2zm5 0v2H7V9h2zm5 0v2h-2V9h2zm-3-5a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1V4zm1 4a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1h-2z"/></svg>`,
      //     title: 'Horizontal Layout',
      // },
      // {
      //     name: 'layoutSquare',
      //     icon: `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-square" viewBox="0 0 16 16"><path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/></svg>`,
      //     title: 'Square Layout',
      // },
      // {
      //     name: 'layoutWithGap',
      //     icon: `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-columns-gap" viewBox="0 0 16 16"><path d="M6 1v3H1V1h5zM1 0a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1H1zm14 12v3h-5v-3h5zm-5-1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1h-5zM6 8v7H1V8h5zM1 7a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1V8a1 1 0 0 0-1-1H1zm14-6v7h-5V1h5zm-5-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1h-5z"/></svg>`,
      //     title: 'Layout With Gap',
      // },
      // {
      //     name: 'layoutWithFixedSize',
      //     icon: `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-grid-3x3" viewBox="0 0 16 16"><path d="M0 1.5A1.5 1.5 0 0 1 1.5 0h13A1.5 1.5 0 0 1 16 1.5v13a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 14.5v-13zM1.5 1a.5.5 0 0 0-.5.5V5h4V1H1.5zM5 6H1v4h4V6zm1 4h4V6H6v4zm-1 1H1v3.5a.5.5 0 0 0 .5.5H5v-4zm1 0v4h4v-4H6zm5 0v4h3.5a.5.5 0 0 0 .5-.5V11h-4zm0-1h4V6h-4v4zm0-5h4V1.5a.5.5 0 0 0-.5-.5H11v4zm-1 0V1H6v4h4z"/></svg>`,
      //     title: 'Layout With Fixed Size',
      // },
    ];

    /**
     * Save block index
     */
    this.blockIndex = this.api.blocks.getCurrentBlockIndex();
    this.api.blocks.stretchBlock(this.blockIndex, true);
    console.log('>> this.blockIndex', this.api.blocks.stretchBlock, this.blockIndex);

    this.uploader = new Uploader({
      config: this.config,
      onUpload: response => this.onUpload(response),
      onError: error => this.uploadingFailed(error)
    });
  }

  /**
   * Creates a Block:
   *  1) Creates a textarea to paste images urls
   *  2) Start to load all urls
   *  3) After loading, append images to gallery
   *
   * @public
   */
  render() {
    this.wrapper = document.createElement('div');
    this.wrapper.classList.add('image-gallery');

    let urls;

    // this.wrapper.appendChild(textarea);
    //
    const button = document.createElement('div');
    button.className = 'cdx-button';
    button.innerHTML = `${buttonIcon} Select an Image`;
    button.addEventListener('click', () => {
      this.uploader.uploadSelectedFile({
        onPreview: src => {
          // console.log("@onPreview")
          // this.ui.showPreloader(src);
        }
      });
    });
    //

    this.nodes.button = button;
    this.nodes.wrapper = this.wrapper;
    this.nodes.urls = urls;

    if (this.data && this.data.urls && this.data.urls.length) {
      this._imageGallery(this.data.urls);
      this.wrapper.appendChild(button);
      return this.wrapper;
    }
    this.wrapper.appendChild(button);
    return this.wrapper;
  }

  /**
   * @public
   * @param {Element} blockContent - Tool's wrapper
   * @returns {ImageGalleryData}
   */
  save(blockContent) {
    console.log('save', blockContent);

    const box = document.createElement('div');
    box.className = 'gg-box-view';
    this._createHtml(box, false);

    return Object.assign(this.data, {
      // ghtml: box.outerHTML,
      images: this.images,
      rows: this.rows
    });
  }

  validate(savedData) {
    console.log('validate', savedData);
    // let imagesToSave = [];
    // let indexCount = 0;

    // savedData.urls.forEach(img => {
    //   if (img.url && img.url.trim() !== '' && this._isImgUrl(img.url)) {
    //     imagesToSave[indexCount] = img;
    //     indexCount++;
    //   }
    // });

    // savedData.urls = imagesToSave;

    return true;
  }

  /**
   * File uploading callback
   *
   * @private
   *
   * @param {UploadResponseFormat} response - uploading server response
   * @returns {void}
   */
  onUpload(response) {
    if (response.success && response.files) {
      const images = response.files;
      this._imageGallery(images, true);
    } else {
      this.uploadingFailed('incorrect response: ' + JSON.stringify(response));
    }
  }

  /**
   * Handle uploader errors
   *
   * @private
   * @param {string} errorText - uploading error text
   * @returns {void}
   */
  uploadingFailed(errorText) {
    console.log('Image Tool: uploading failed because of', errorText);

    // this.api.notifier.show({
    // message: this.api.i18n.t('Couldn’t upload image. Please try another.'),
    // style: 'error',
    // });
    // this.ui.hidePreloader();
  }

  /**
   * Sanitizer rules
   */
  static get sanitize() {
    return {
      urls: {},
      editImages: {},
      bkgMode: {},
      layoutDefault: {},
      layoutHorizontal: {},
      layoutSquare: {},
      layoutWithGap: {},
      layoutWithFixedSize: {}
    };
  }

  /**
   * Returns images data
   *
   * @returns {ImageGalleryData}
   */
  get data() {
    return this._data;
  }

  /**
   * Set images data and update the view
   *
   * @param {ImageGalleryData} data
   */
  set data(data) {
    this._data = Object.assign({}, this.data, data);

    if (this.nodes.urls) {
      this.nodes.urls = this.data.urls;
    }
  }

  /**
   * Fires after clicks on the Toolbox Image Icon
   * Initiates click on the Select File button
   *
   * @public
   */
  appendCallback() {
    this.nodes.button.click();
  }

  /**
   * Makes buttons with tunes: background mode, layout default, layout horizontal,
   * square layout, layout with gap and layout with fixed size
   *
   * @returns {HTMLDivElement}
   */
  renderSettings() {
    const wrp = document.createElement('div');

    this.settings.forEach(tune => {
      let button = document.createElement('div');

      button.classList.add('cdx-settings-button');
      button.innerHTML = tune.icon;
      button.title = tune.title;
      wrp.appendChild(button);

      button.addEventListener('click', () => {
        this._toggleTune(tune.name);
        button.classList.toggle('cdx-settings-button--active');
      });
    });

    return wrp;
  }

  /**
   * @private
   * Click on the Settings Button
   * @param {string} tune — tune name from this.settings
   */
  _toggleTune(tune) {
    if (tune === 'bkgMode' || tune === 'editImages') {
      this.data[tune] = !this.data[tune];
    } else {
      this.settings.forEach(t => {
        if (t.name !== 'bkgMode' && t.name !== 'editImages') {
          if (t.name === tune) this.data[t.name] = true;
          else this.data[t.name] = false;
        }
      });
    }

    this._acceptTuneView();
  }

  /**
   * Add specified logic corresponds with activated tunes
   * @private
   */
  _acceptTuneView() {
    let gallery = document.querySelector('div#image-gallery-' + this.blockIndex + ' > div.gg-box');
    let urlsInput = document.querySelector('textarea.image-gallery-' + this.blockIndex);

    if (gallery !== null) {
      gallery.getAttributeNames().forEach(attr => {
        gallery.removeAttribute(attr);
      });

      gallery.className = '';
      gallery.classList.add('gg-box');
      gallery.id = '';

      this.settings.forEach(tune => {
        switch (tune.name) {
          // case 'editImages':
          //     if (this.data.editImages && urlsInput.classList.contains('textarea-hide'))
          //         urlsInput.classList.remove('textarea-hide');
          //     else if (!this.data.editImages && !urlsInput.classList.contains('textarea-hide'))
          //         urlsInput.classList.add('textarea-hide');
          //     break;
          case 'bkgMode':
            if (this.data.bkgMode) {
              gallery.classList.add('dark');
              gg.galleryOptions({
                selector: '.dark',
                darkMode: true
              });
            }
            break;
          case 'layoutDefault':
            break;
          case 'layoutHorizontal':
            if (this.data.layoutHorizontal) {
              gallery.id = 'horizontal';
              gg.galleryOptions({
                selector: '#horizontal',
                layout: 'horizontal'
              });
            }
            break;
          case 'layoutSquare':
            if (this.data.layoutSquare) {
              gallery.id = 'square';
              gg.galleryOptions({
                selector: '#square',
                layout: 'square'
              });
            }
            break;
          case 'layoutWithGap':
            if (this.data.layoutWithGap) {
              gallery.id = 'gap';
              gg.galleryOptions({
                selector: '#gap',
                gapLength: 10
              });
            }
            break;
          case 'layoutWithFixedSize':
            if (this.data.layoutWithFixedSize) {
              gallery.id = 'heightWidth';
              gg.galleryOptions({
                selector: '#heightWidth',
                rowHeight: 180,
                columnWidth: 280
              });
            }
            break;
        }
      });
    }
  }

  _createHtml(box, editor = false) {
    this.images = this.urls.filter(img => this._isImgUrl(img.url) != null);
    if (this.images.length >= 9) {
      this.images = this.images.slice(0, 9);
      this.nodes.button.classList.add('gg-hide');
    } else {
      this.nodes.button.classList.remove('gg-hide');
    }
    // ---
    let rows = [[]];
    let f1 = 0;
    let count = this.images.length;

    let row = function(e, t) {
      if (e + 1 === t) return false;
      if (t > 1 && t % 3 == 1) {
        return t === 4 ? e % 2 : e === 2 || (e > 2 && (e + 1) % 2);
      } else {
        return (e + 1) % 3 === 0;
      }
    };
    // ---
    this.images.forEach((img, i) => {
      rows[f1].push(img);

      const u = (img.width || 1) / (img.height || 1);
      img.flex = `flex: ${u} 1 0%`;
      if (row(i, count)) {
        f1++;
        rows[f1] = [];
      }
    });

    this.rows = rows;

    rows.forEach(row => {
      const _row = document.createElement('div');
      _row.className = 'gg-row';

      row.forEach(col => {
        const _col = document.createElement('div');
        _col.className = 'gg-col';
        _col.style = col.flex;
        //
        const img = document.createElement('img');
        img.src = col.url;
        //
        const _overlay = document.createElement('div');
        _overlay.className = 'gg-image-overlay';
        //
        const _button = document.createElement('button');
        _button.className = 'gg-image-overlay-button';
        _button.innerHTML = buttonTrashIcon;
        _button.addEventListener('click', () => {
          const index = this.images.findIndex(i => i.id === col.id);
          if (index !== -1) {
            this.images.splice(index, 1);
            this._imageGallery(this.images);
          }
        });
        //
        _overlay.appendChild(_button);
        _col.appendChild(img);
        _col.appendChild(_overlay);

        _row.appendChild(_col);

        Sortable.create(_row, {
          group: 'shared',
          swap: true,
          animation: 150,
          swapThreshold: 0.22,
          onEnd: function(/**Event*/ evt) {
            console.log('onEnd', evt);
          }
        });
      });
      box.appendChild(_row);
    });
  }
  /**
   * @private
   * Create image gallery
   * @param {string[]} urls — list of gallery url images
   */
  _imageGallery(images = [], concat = false) {
    if (concat) {
      this.urls = this.urls.concat(images);
    } else {
      this.urls = images;
    }

    const oldContainers = document.querySelectorAll('#image-gallery-' + this.blockIndex);
    oldContainers.forEach(oldContainer => {
      if (oldContainer !== null) {
        oldContainer.remove();
      }
    });

    const box = document.createElement('div');
    box.className = 'gg-box';

    this._createHtml(box, true);

    const gallery = document.createElement('div');
    gallery.className = 'gg-container';
    gallery.id = 'image-gallery-' + this.blockIndex;
    gallery.appendChild(box);

    //this.wrapper.innerHTML = ''; // Hide url input after paste

    if (this.images.length > 0) this.wrapper.prepend(gallery);

    // gg.loadGallery();

    this._acceptTuneView();
  }

  /**
   * @private
   * Validate image url
   * @param {string} imgUrl — Image URL to be validated
   */
  _isImgUrl(imgUrl) {
    return imgUrl.match(/https?:\/\/\S+\.(gif|jpe?g|tiff|png|webp)$/i);
  }
}

export default ImageGallery;
