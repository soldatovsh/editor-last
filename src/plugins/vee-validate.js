// https://codesandbox.io/s/validation-components-bootstrapvue-usdwv?from-embed
import Vue from 'vue';
import { extend, localize, ValidationProvider, ValidationObserver, setInteractionMode } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';
import ru from 'vee-validate/dist/locale/ru';

// import {NUMBER, LETTER, mobile, phone} from '../../shared/utils/validation'

// import axios from "axios";

for (const rule in rules) {
  extend(rule, rules[rule]);
}

setInteractionMode('betterPassive', ({ errors, flags, value }) => {
  // console.log(errors.length, errors, flags, value);
  if (errors.length) {
    return {
      on: ['input', 'change'],
      debounce: 350
    };
  }

  return {
    on: ['change', 'blur']
  };
});

const ValidationObserverExtended = ValidationObserver.extend({
  methods: {
    // setErrors(errors) {
    //   Object.keys(errors).forEach(key => {
    //     const provider = this.refs[key];
    //     if (!provider) {
    //       console.warn(`[!] Сервер вернул ошибку для поля "${key}", но поле не найдено`);
    //       return;
    //     }
    //     let errorArr = errors[key] || [];
    //     errorArr = typeof errorArr === 'string' ? [errorArr] : errorArr;
    //     provider.setErrors(errorArr);
    //   });
    //   this.observers.forEach(observer => {
    //     observer.setErrors(errors);
    //   });
    // }
  }
});

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserverExtended);

localize('ru', ru);

// extend("serverUniqueEmailOrPhone", {
//   validate: async (value, {needUnique = true}) => {
//     try{
//       const { data: registerData } = await axios.post('/api/validators/checkTakenEmailOrPhone', {
//         emailOrPhone: value
//       });

//       if(needUnique !== 'false'){
//         if(registerData.isTaken){
//           throw new Error('Пользователь уже зарегистрирован')
//         }
//       }else{
//         if(!registerData.isTaken){
//           throw new Error('Пользователь не найден')
//         }
//       }
//       return { valid: true }
//     }catch(error){
//       return {
//         valid: false,
//         data: {
//           message: error.message
//         }
//       }
//     }
//   },
//   getMessage: (field, params, data) => {
//     return data.message
//   }
// }, {paramNames:['needUnique']});

// extend("checked", {
//   validate: (value) => {
//     return  { valid: value };
//   },
//   getMessage: ru.messages.required
// });

// extend("phone", {
//   validate: (value) => {
//     if(!phone(value)) return { valid: true }
//     return { valid: false };
//   },
//   getMessage: () => 'Некорректный номер телефона'
// });

// extend("mobile", {
//   validate: (value) => {
//     if(!mobile(value)) return { valid: true }
//     return { valid: false };
//   },
//   getMessage: () => 'Некорректный номер телефона'
// });

// extend("requiredSearch", {
//   validate: (value) => {
//     if(!value?.value) return { valid: false }
//     return { valid: true };
//   },
//   getMessage: ru.messages.required
// });

// extend("password", {
//   validate: (value) => {
//     const letters = value.match(LETTER);
//     const numbers = value.match(NUMBER);
//     if (letters && numbers && letters.length > 0 && numbers.length > 0) return { valid: true };
//     return { valid: false };
//   },
//   getMessage: () => 'Пароль должен содержать буквы и цифры'
// });

// extend("serverCurrentPassword", {
//   validate: async (value, {needUnique = true}) => {
//     try{
//       const { data } = await axios.post('/api/validators/currentPassword', {
//         password: value
//       });

//       return { valid: true }
//     }catch(error){
//       return {
//         valid: false,
//         data: {
//           message: error?.response?.data?.message || error.message
//         }
//       }
//     }
//   },
//   getMessage: (field, params, data) => {
//     return data.message
//   }
// }, {paramNames:['needUnique']});
