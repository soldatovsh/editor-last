import Vue from 'vue';

import { BootstrapVue } from 'bootstrap-vue';
import config from '~/configs/bootstrap-vue';

// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue, config);
