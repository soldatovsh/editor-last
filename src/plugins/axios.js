// https://github.com/nuxt-community/axios-module/blob/master/lib/plugin.js
import Vue from 'vue';
import Axios from 'axios';

const { PROXY } = require('~/configs/app');


const BASE_URL = (process.env.NODE_ENV === 'development' && window.location.host != 'coin.test') ? PROXY : '';

const axiosExtra = {
  setBaseURL(baseURL) {
    this.defaults.baseURL = baseURL;
  },
  setHeader(name, value, scopes = 'common') {
    for (const scope of Array.isArray(scopes) ? scopes : [scopes]) {
      if (!value) {
        delete this.defaults.headers[scope][name];
        return;
      }
      this.defaults.headers[scope][name] = value;
    }
  },
  setToken(token, type, scopes = 'common') {
    const value = !token ? null : (type ? `${type} ` : '') + token;
    this.setHeader('Authorization', value, scopes);
  },
  onRequest(fn) {
    this.interceptors.request.use(config => fn(config) || config);
  },
  onResponse(fn) {
    this.interceptors.response.use(response => fn(response) || response);
  },
  onRequestError(fn) {
    this.interceptors.request.use(undefined, error => fn(error) || Promise.reject(error));
  },
  onResponseError(fn) {
    this.interceptors.response.use(undefined, error => fn(error) || Promise.reject(error));
  },
  onError(fn) {
    this.onRequestError(fn);
    this.onResponseError(fn);
  },
  create(options) {
    return createAxiosInstance(defu(options, this.defaults));
  }
};

// Request helpers ($get, $post, ...)
for (const method of ['request', 'delete', 'get', 'head', 'options', 'post', 'put', 'patch']) {
  axiosExtra[`$${method}`] = function() {
    return this[method].apply(this, arguments).then(res => res && res.data);
  };
}

const extendAxiosInstance = axios => {
  for (const key in axiosExtra) {
    axios[key] = axiosExtra[key].bind(axios);
  }
};

const createAxiosInstance = axiosOptions => {
  // Create new axios instance
  const axios = Axios.create(axiosOptions);
  axios.CancelToken = Axios.CancelToken;
  axios.isCancel = Axios.isCancel;

  // Extend axios proto
  extendAxiosInstance(axios);

  return axios;
};

const axiosOptions = {
  baseURL: BASE_URL,
  headers: {}
};

const axios = createAxiosInstance(axiosOptions);

axios.onRequest(config => {
  // config.headers.common["Cache-Control"] = "no-cache";
  // if (store.getters["auth/token"]) {
  // config.headers.common["Authorization"] = `Bearer ${store.getters["auth/token"]}`;
  // }
});

axios.onError(error => {
  console.error('$axios.error', error);
  // const isAuth = store.getters["auth/check"];
  const code = parseInt(error?.response?.status);
  if (code === 401) {
    window.App.showModal('modal-auth');
  }

  // if (isAuth && code === 401) {
  // store.dispatch("auth/logout");
  // app.router.push({name:'auth-login'})
  // }
});

const plugin = async (Vue, axios) => {
  if (plugin.installed) {
    return;
  }
  plugin.installed = true;

  Vue.$axios = axios;
  Object.defineProperties(Vue.prototype, {
    $axios: {
      get() {
        return axios;
      }
    }
  });
};

Vue.use(plugin, axios);
